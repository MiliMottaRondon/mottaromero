import java.util.*;

interface HashTreeSet {
  public void addNode(HashTree ht, Node node);									
  public void deleteNode(HashTree ht, Node node); 
  public void splitHash(HashTree ht);
}

class Node {
  int valor;
  Node left = null;
  Node right = null;

  public Node(int valor, Node left, Node right) {
    this.valor = valor;
    this.left = left;
    this.right = right;
  }
  public Node(int value) {
    this(value, null, null);
  }
}

class HashTree<T, E> extends HashSet implements HashTreeSet {
  private int size;
  private Node node;
  private MyIterator iterator;
  HashSet<T> datos;
  
  public HashTree(int size) {
    this.size = size;
    datos = new HashSet<T>();
    iterator = new MyIterator();
    node=null;
  }
  
  public boolean isEmpty() {
    return false;
  }

  public boolean contains(Object o) {
    return true;
  }

  public Iterator<E> iterator() {
    return null;
  }

  public boolean remove(Object o) {
    return true;
  }

  public void clear() {
  }

  public Object clone() {
    return null;
  }
  public HashSet getHashSet(){
    return datos;
  }
  public class MyIterator<T> implements Iterator<T> {
    private int index;

    public MyIterator() {
      index = 0;
    }

    @Override
    public boolean hasNext() {
      return index < size();
    }

    @Override
    public T next() {
      return null;
    }

    @Override
    public void remove() {
    }
  }

  @Override
  public void addNode(HashTree ht, Node node) {
    assert ht.getHashSet.size() == size;
    // We go through all the HashSet with the iterator
    while(iterator.hasNext()){
      // We choose the size/2 major or minor number and upload it to a node with the splitHash
    }
  }

  @Override
  public void deleteNode(HashTree ht, Node node) {}

  @Override
  public void splitHash(HashTree ht) {}
  
}